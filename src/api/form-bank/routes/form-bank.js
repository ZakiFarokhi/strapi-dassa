'use strict';

/**
 * form-bank router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::form-bank.form-bank');
