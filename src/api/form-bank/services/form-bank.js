'use strict';

/**
 * form-bank service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::form-bank.form-bank');
