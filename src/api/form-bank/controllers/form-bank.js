'use strict';

/**
 * form-bank controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::form-bank.form-bank');
