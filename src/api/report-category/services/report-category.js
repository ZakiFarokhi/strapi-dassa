'use strict';

/**
 * report-category service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::report-category.report-category');
