'use strict';

/**
 * report-category router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::report-category.report-category');
