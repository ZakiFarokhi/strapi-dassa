'use strict';

/**
 * report-category controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::report-category.report-category');
