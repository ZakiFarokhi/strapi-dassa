import type { Schema, Attribute } from '@strapi/strapi';

export interface ImagesCarousel extends Schema.Component {
  collectionName: 'components_images_carousels';
  info: {
    displayName: 'carousel';
    icon: 'picture';
  };
  attributes: {
    url: Attribute.String;
    image: Attribute.Media;
  };
}

declare module '@strapi/types' {
  export module Shared {
    export interface Components {
      'images.carousel': ImagesCarousel;
    }
  }
}
